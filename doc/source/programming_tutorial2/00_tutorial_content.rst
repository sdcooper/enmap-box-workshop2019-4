
2. Graphical User Interfaces
############################

Overview
========

This tutorial gives an introduction how to create graphical user interfaces with Qt, QGIS and the EnMAP-Box.

It addresses users that never created a GUI programmatically before. Experienced users might
skip single exercises and continue with those of their specific interest.

The tutorial will cover the following aspects:

#. Basics GUI programming. Here you learn the very fundamentals how to create own GUI applications

#. EnMAP-Box Applications. Here you learn how to create an Application that appears in the EnMAP-Box

#. Advanced GUI programming. Here you find several examples how to solve some more specific problems




.. include:: 01_basic_examples.rst
.. include:: 02_enmapbox_applications.rst
.. include:: 03_advanced_examples.rst
