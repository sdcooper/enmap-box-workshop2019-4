
import os, sys, unittest
from qgis.PyQt.QtCore import *
from qgis.PyQt.QtGui import *
from qgis.PyQt.QtWidgets import *
import qgis.PyQt.uic
from qgis.core import *
from qgis.gui import *


from enmapbox.testing import initQgisApplication

class BasicExamples(unittest.TestCase):

    def test_example1_hello_world(self):

        from PyQt5.QtWidgets import QApplication, QWidget

        app = QApplication([])
        
        widget = QWidget()
        widget.setWindowTitle('Hello World')
        widget.show()

        app.exec_()


    def test_example2_hello_real_world(self):

        from enmapbox.testing import initQgisApplication
        app = initQgisApplication()

        assert isinstance(app, QApplication)
        assert isinstance(app, QgsApplication)

        uri = r'crs=EPSG:3857&format&type=xyz&url=https://mt1.google.com/vt/lyrs%3Ds%26x%3D%7Bx%7D%26y%3D%7By%7D%26z%3D%7Bz%7D&zmax=19&zmin=0'
        layer = QgsRasterLayer(uri, 'google maps', 'wms')

        import enmapboxtestdata
        #layer = QgsRasterLayer(enmapboxtestdata.enmap)

        assert layer.isValid()


        from qgis.gui import QgsMapCanvas
        canvas = QgsMapCanvas()
        canvas.setWindowTitle('Hello Real World')
        canvas.setLayers([layer])
        canvas.setExtent(layer.extent())
        canvas.setDestinationCrs(layer.crs())
        #canvas.setDestinationCrs(QgsCoordinateReferenceSystem('EPSG:4326'))
        canvas.setDestinationCrs(QgsCoordinateReferenceSystem('EPSG:32632'))
        canvas.show()

        app.exec_()

    def test_examples3_and_4(self):

        app = initQgisApplication()

        myWidget = ExampleWidget()
        myWidget.show()

        app.exec_()


    def test_example5_form_files(self):

        app = initQgisApplication()

        w = ExampleUI()
        w.show()

        app.exec_()




    def test_example4_signals_minimumexample(self):


        app = initQgisApplication()

        button = QPushButton()
        button.setText('Press me')

        def onClicked(*args):
            print('Button was pressed')

        button.clicked.connect(onClicked)
        button.show()

        app.exec_()

    def test_example3_checkedButtons(self):

        app = initQgisApplication()

        frame = QFrame()
        frame.setLayout(QHBoxLayout())

        def onClicked(checked):
            sender = QApplication.sender()
            assert isinstance(sender, QPushButton)
            print('Button {} checked: {}'.format(sender.text(), checked))

        button1 = QPushButton('B1')
        button1.clicked.connect(onClicked)

        button2 = QPushButton('B2')
        button2.setCheckable(True)
        button2.setChecked(True)
        button1.clicked.connect(onClicked)

        frame.layout().addWidget(button1)
        frame.layout().addWidget(button2)

        frame.show()
        app.exec_()





class ExampleWidget(QWidget):

    def __init__(self, parent=None):
        super(ExampleWidget, self).__init__(parent)

        self.setWindowTitle('Example Widget')
        self.resize(QSize(300, 200))

        self.textBox = QTextEdit()
        self.mapCanvas = QgsMapCanvas()
        self.label = QLabel('Label info')
        self.button = QPushButton('Press me')

        self.setLayout(QVBoxLayout())

        self.topLayout = QHBoxLayout()
        self.topLayout.addWidget(self.textBox)
        self.topLayout.addWidget(self.mapCanvas)

        self.bottomLayout = QHBoxLayout()
        self.bottomLayout.addWidget(self.label)
        self.bottomLayout.addWidget(self.button)

        self.layout().addLayout(self.topLayout)
        self.layout().addLayout(self.bottomLayout)

        from enmapboxtestdata import enmap
        layer = QgsRasterLayer(enmap)
        QgsProject.instance().addMapLayer(layer)
        self.mapCanvas.setLayers([layer])
        self.mapCanvas.setDestinationCrs(layer.crs())
        self.mapCanvas.setExtent(self.mapCanvas.fullExtent())

        # connect the signal 'clicked' with slot 'onButtonClicked'
        self.button.clicked.connect(self.onButtonClicked)

        # connect the signal 'clicked' with a lambda function
        self.button.clicked.connect(lambda : self.mapCanvas.setExtent(self.mapCanvas.fullExtent()))



    def resetMap(self):
        layers = self.mapCanvas.layers()
        if len(layers) > 0:
            layer = layers[0]
            assert isinstance(layer, QgsMapLayer)
            self.mapCanvas.setDestinationCrs(layer.crs())
            self.mapCanvas.setExtent(self.mapCanvas.fullExtent())


    def onButtonClicked(self, *args):
        text = self.textBox.toPlainText()
        text = text + '\nclicked'
        self.textBox.setText(text)

        from datetime import datetime
        self.label.setText('Last click {}'.format(datetime.now().time()))

    sigMouseMoved = pyqtSignal([],[int, int], [str])




class ExampleMapCanvas(QgsMapCanvas):

    def __init__(self, *args, **kwds):
        super(ExampleMapCanvas, self).__init__(*args, **kwds)

    def mouseMoveEvent(self, mouseEvent:QMouseEvent):
        super(ExampleMapCanvas, self).mouseMoveEvent(mouseEvent)



pathUi = os.path.join(os.path.dirname(__file__), 'exampleform.ui')
from enmapbox.gui.utils import loadUIFormClass
class ExampleUI(QWidget, loadUIFormClass(pathUi)):

    def __init__(self, parent=None):
        super(ExampleUI, self).__init__(parent)
        self.setupUi(self)

        assert isinstance(self.mapCanvas, QgsMapCanvas)

        self.initActions()
        self.mapCanvas.contextMenuEvent = self.canvasContextMenuEvent


    def initActions(self):

        self.actionZoomOut.triggered.connect(lambda: self.setMapTool('ZOOM_IN'))
        self.actionZoomOut.triggered.connect(lambda: self.setMapTool('ZOOM_OUT'))
        self.actionZoomFull.triggered.connect(lambda: self.mapCanvas.setExtent(self.mapCanvas.fullExtent()))
    def canvasContextMenuEvent(*args):

        s = ""

    def setMapTool(self, key):

        assert isinstance(self.mapCanvas, QgsMapCanvas)
        if key == 'ZOOM_IN':
            mt = QgsMapToolZoom(self.mapCanvas, False)
            self.mapCanvas.setMapTool(mt)
        elif key == 'ZOOM_OUT':
            mt = QgsMapToolZoom(self.mapCanvas, True)
            self.mapCanvas.setMapTool(mt)




