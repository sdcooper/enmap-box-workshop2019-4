Day 1 (Mon 25.02.2019)
######################

13:00 Registration & Coffee
===========================

:room: 0'119, Erwin-Schrödinger-Zentrum, Rudower Chaussee 26, 12489 Berlin

Doors open at 12:00, in case you want to be there earlier. There is also a
café right next to the room with several sitting accommodations.

.. image:: ../img/map_room.png
   :width: 75%


13:30 Welcome and Introduction
==============================
:room: 0'119

13:40 EnMAP Mission
===================
:room: 0'119

Overview and Updates

*Saskia Förster, GFZ Potsdam*

14:00 EnMAP-Box Concept, Progress & Next Steps
==============================================
:room: 0'119

*Sebastian van der Linden, HU Berlin*

14:30 Application Tutorial 1
============================
:room: 0'119

Regression based unmixing

15:00 Coffee Break
==================

in front of room 0'119

.. attention:: After the coffee break the workshop continues in the Geography Department

15:30 Application Tutorial 1 (part 2)
=====================================


:room: 1'231 and 1'230 (two groups), Geography Department, Rudower Chaussee 16, 12489 Berlin

Continuation of Regression based unmixing tutorial

16:30 Application Tutorial 2 & 3
================================

:room: 1'231 and 1'230 (two groups)



